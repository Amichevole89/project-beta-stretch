# from django.contrib import admin
# from .models import AutomobileVO, Customer, Salesperson, Sale


# @admin.register(AutomobileVO)
# class AutomobileVOAdmin(admin.ModelAdmin):
#     list_display = (
#         "id",
#         "import_href",
#         "vin",
#     )


# @admin.register(Customer)
# class CustomerAdmin(admin.ModelAdmin):
#     list_display = (
#         "id",
#         "name",
#         "phone",
#         "address",
#     )


# @admin.register(Salesperson)
# class SalespersonAdmin(admin.ModelAdmin):
#     list_display = (
#         "id",
#         "name",
#         "employee_number",
#     )


# @admin.register(Sale)
# class SaleAdmin(admin.ModelAdmin):
#     list_display = (
#         "id",
#         "automobile.vin",
#         "automobile.id",
#         "salesperson.id",
#         "customer.id",
#         "price",
#     )
