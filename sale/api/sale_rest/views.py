import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Customer, Salesperson, Sale
from .encoders import (
    ListCustomersEncoder,
    ListSalespersonsEncoder,
    SaleEncoder,
)


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        return JsonResponse(
            {"customer": Customer.objects.all()},
            encoder=ListCustomersEncoder,
        )
    content = json.loads(request.body)
    return JsonResponse(
        Customer.objects.create(**content),
        encoder=ListCustomersEncoder,
        safe=False,
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def single_customer(request, pk):
    if request.method == "GET":
        return JsonResponse(
            Customer.objects.get(id=pk),
            encoder=ListCustomersEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        return JsonResponse(
            Customer.objects.get(id=pk),
            encoder=ListCustomersEncoder,
            safe=False,
        )
    count, _ = Customer.objects.filter(id=pk).delete()
    return JsonResponse(
        {"Customer successfully deleted": count > 0},
    )


@require_http_methods(["GET", "POST"])
def list_salespersons(request):
    if request.method == "GET":
        return JsonResponse(
            {"salespersons": Salesperson.objects.all()},
            encoder=ListSalespersonsEncoder,
            safe=False,
        )
    content = json.loads(request.body)
    return JsonResponse(
        Salesperson.objects.create(**content),
        encoder=ListSalespersonsEncoder,
        safe=False,
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def single_salesperson(request, pk):
    if request.method == "GET":
        return JsonResponse(
            Salesperson.objects.get(id=pk),
            encoder=ListSalespersonsEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Salesperson.objects.filter(id=pk).update(**content)
        return JsonResponse(
            Salesperson.objects.get(id=pk),
            encoder=ListSalespersonsEncoder,
            safe=False,
        )
    count, _ = Salesperson.objects.filter(id=pk).delete()
    return JsonResponse(
        {"Salesperson successfully deleted": count > 0},
    )


@require_http_methods(["GET", "POST"])
def list_sales(request, salesperson_id=None):
    if request.method == "GET":
        sales = Sale.objects.all()
        if salesperson_id:
            sales = Sale.objects.filter(salesperson=salesperson_id)
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False,
        )
    content = json.loads(request.body)
    try:
        content["customer"] = Customer.objects.get(id=content["customer"])
        content["salesperson"] = Salesperson.objects.get(
            id=content["salesperson"]
        )
        content["automobile"] = AutomobileVO.objects.get(
            id=content["automobile"]
        )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    except Exception as e:
        return JsonResponse(
            {f"{e} error": "Double check the information you're passing"},
            status=418,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def single_sale(request, pk):
    if request.method == "GET":
        return JsonResponse(
            Sale.objects.get(id=pk),
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "automobile" in content:
                auto = content["automobile"]
                content["automobile"] = AutomobileVO.objects.get(id=auto)
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"Error": "Invalid automobile id"})
        try:
            if "salesperson" in content:
                person = content["salesperson"]
                content["salesperson"] = Salesperson.objects.get(id=person)
        except Salesperson.DoesNotExist:
            return JsonResponse({"Error": "Invalid salesperson id"})
        try:
            if "customer" in content:
                customer = content["customer"]
                content["customer"] = Customer.objects.get(id=customer)
        except Customer.DoesNotExist:
            return JsonResponse({"Error": "Invalid customer id"})
        Sale.objects.filter(id=pk).update(**content)
        return JsonResponse(
            Sale.objects.get(id=pk),
            encoder=SaleEncoder,
            safe=False,
        )
    count, _ = Sale.objects.filter(id=pk).delete()
    return JsonResponse(
        {"Sale successfully deleted": count > 0},
    )
