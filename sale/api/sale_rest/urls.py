from django.urls import path
from .views import (
    list_customers,
    list_sales,
    list_salespersons,
    single_customer,
    single_sale,
    single_salesperson,
)

urlpatterns = [
    path("salesperson/", list_salespersons, name="list_salespersons"),
    path(
        "salesperson/<int:pk>/sales/",
        single_salesperson,
        name="single_salesperson",
    ),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", single_sale, name="single_sale"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:pk>/", single_customer, name="single_customer"),
]
