from django.apps import AppConfig


class SaleRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "sale_rest"
