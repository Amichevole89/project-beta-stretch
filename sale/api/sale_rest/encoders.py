from common.json import ModelEncoder
from .models import AutomobileVO, Customer, Sale, Salesperson


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "import_href",
        "vin",
    ]


class ListCustomersEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone",
    ]


class ListSalespersonsEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "name",
        "employee_number",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "price",
    ]
    encoders = {
        "customer": ListCustomersEncoder(),
        "automobile": AutomobileVOEncoder(),
        "salesperson": ListSalespersonsEncoder(),
    }
