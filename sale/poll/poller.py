import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sale_project.settings")
django.setup()

from sale_rest.models import AutomobileVO


def generate_autos():
    response = requests.get(f"{os.environ['REACT_APP_INVENTORY_API']}/api/automobiles/")
    content = json.loads(response.content)
    for car in content["autos"]:
        AutomobileVO.objects.update_or_create(
            import_href=car["href"],
            defaults={
                "vin": car["vin"],
                "id": car["id"],
            },
        )


def poll():
    while True:
        try:
            generate_autos()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
