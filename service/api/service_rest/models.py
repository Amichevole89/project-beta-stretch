from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=300, unique=True, null=True)
    vin = models.CharField(max_length=17)


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField(null=True, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("list_technicians", kwargs={"pk": self.id})


class Appointment(models.Model):
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.PROTECT
    )
    customer_name = models.CharField(max_length=200)
    VIP_treatment = models.BooleanField(default=False)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    vin = models.CharField(max_length=17)
    purpose = models.TextField()
    completed = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.customer_name} {self.vin}"

    def get_api_url(self):
        return reverse("single_appointment", kwargs={"pk": self.id})
