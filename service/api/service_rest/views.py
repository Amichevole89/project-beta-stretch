from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
import json

from .encoders import (
    TechnicianEncoder,
    AppointmentListEncoder,
    AppointmentDetailEncoder,
)
from .models import Technician, Appointment, AutomobileVO


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            status=200,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
                status=201,
            )
        except ValidationError as e:
            return JsonResponse({"Error": str(e)}, status=400)


@require_http_methods(["GET", "PUT", "DELETE"])
def single_technician(request, pk):
    if request.method == "GET":
        return JsonResponse(
            Technician.objects.get(id=pk),
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        return JsonResponse(Technician.objects.get(id=pk))
    count, _ = Technician.objects.filter(id=pk).delete()
    return JsonResponse(
        {"Technician successfully deleted": count > 0},
    )


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            status=200,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.get(id=content["technician"])
        content["technician"] = technician
        try:
            AutomobileVO.objects.get(vin=content["vin"])
            content["VIP_treatment"] = True
        except AutomobileVO.DoesNotExist:
            content["VIP_treatment"] = False
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
            status=201,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def single_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
            status=200,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(id=content["technician"])
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"Error": "Invalid Technician, please select one that exists"},
                status=400,
            )
        Appointment.objects.filter(id=pk).update(**content)
        return JsonResponse(
            Appointment.objects.get(id=pk),
            encoder=AppointmentDetailEncoder,
            safe=False,
            status=200,
        )

    count, _ = Appointment.objects.filter(id=pk).delete()
    return JsonResponse(
        {"Appointment successfully deleted": count > 0}, status=204
    )
