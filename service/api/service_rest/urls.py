from django.urls import path

from .views import list_appointments, single_appointment, list_technicians

urlpatterns = [
    path(
        "appointments/",
        list_appointments,
        name="list_appointments",
    ),
    path(
        "appointments/<int:pk>/",
        single_appointment,
        name="single_appointment",
    ),
    path(
        "technicians/",
        list_technicians,
        name="list_technicians",
    ),
]
