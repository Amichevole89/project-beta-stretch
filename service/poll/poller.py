import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
# from service_rest.models import Something


from service_rest.models import AutomobileVO


def getAutomobiles():
    url = f"{os.environ['REACT_APP_INVENTORY_API']}/api/automobiles/"

    response = requests.get(url)
    content = json.loads(response.content)
    for vehicle in content["autos"]:
        AutomobileVO.objects.update_or_create(
            import_href=vehicle["href"],
            defaults={"vin": vehicle["vin"]},
        )


def poll():
    while True:
        print("Service poller polling for data")
        try:
            getAutomobiles()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
