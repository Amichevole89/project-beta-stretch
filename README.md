# CarCar

Team:

Henry Tran -[Sales](./sales/)
Tyler Male -[Service](./service/)

## Design

Project beta is a small-scale dealership website that handles sales, service, and inventory.

Sales and Service are microservices which are children of the Inventory.

Both Inventory and Service have Modal forms instead of having form pages.  All forms will be accessible and State is updated automatically with props upon successful form submission.

## Service microservice
This microservice will have a AutomobileVO with vin and import_href to get vin and href from an automobile which exists within the parent, Inventory.  This microservice also has Appointment and Technician models.  Appointment has a foreign key to technician which protects upon deletion.

The data will be coming from the RESTful API which will be wired to the react app.

Most of the components within Service are written with functional components with hooks.

Service checks if an automobile has ever been sold by the dealership, which is handled by the Appointment model.  This model updates a VIP_treatment field which carries over into service appointments.

There is also an option to see completed service appointments, or cancel service appointments.

## Sales microservice

The poller in the sales microservice polls for automobiles from the inventory and makes automobile value objects in the sales microservice.
The models in the sales microservice consist of AutomobileVO, SalesPerson, Customer, and SaleRecord. In the models, there are fields that can be used as unique identifiers such as the VIN field in the AutomobileVO and employee_number field in SalesPerson.
