import React from "react";

const LoginNav = () => {
  return (
    <div className='dropdown dropstart'>
      <button
        type='button'
        className='btn btn-primary dropdown-toggle'
        data-bs-toggle='dropdown'
        aria-expanded='false'
        data-bs-auto-close='outside'
      >
        Login
      </button>
      <form className='dropdown-menu p-4'>
        <div className='mb-3'>
          <label htmlFor='exampleDropdownFormEmail2' className='form-label'>
            Email address
          </label>
          <input
            type='email'
            className='form-control-lg'
            id='exampleDropdownFormEmail2'
            placeholder='email@example.com'
          />
        </div>
        <div className='mb-3'>
          <label htmlFor='exampleDropdownFormPassword2' className='form-label'>
            Password
          </label>
          <input
            type='password'
            className='form-control-lg'
            id='exampleDropdownFormPassword2'
            placeholder='Password'
          />
        </div>
        <div className='mb-3'>
          <div className='form-check'>
            <input
              type='checkbox'
              className='form-check-input'
              id='dropdownCheck2'
            />
            <label className='form-check-label' htmlFor='dropdownCheck2'>
              Remember me
            </label>
          </div>
        </div>
        <button type='submit' className='btn btn-primary'>
          Sign in
        </button>
      </form>
    </div>
  );
};

export default LoginNav;
