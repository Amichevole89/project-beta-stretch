import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
// import TechnicianForm from "./Components/Services/TechnicianForm";
// import AppointmentForm from "./Components/Services/AppointmentForm";
import AppointmentList from "./Components/Services/AppointmentList";
import AppointmentHistory from "./Components/Services/AppointmentHistory";
import SalesPersonForm from "./Components/Sale/SalesPersonForm";
import SaleForm from "./Components/Sale/SaleForm";
import CustomerForm from "./Components/Sale/CustomerForm";
import SaleList from "./Components/Sale/SaleList";
import SaleHistory from "./Components/Sale/SaleHistory";
import ManufacturerForm from "./Components/Inventory/ManufacturerForm";
import ManufacturerList from "./Components/Inventory/ManufacturerList";
import AutomobilesList from "./Components/Inventory/AutomobilesList";
import AutomobileForm from "./Components/Inventory/AutomobileForm";
import VehicleForm from "./Components/Inventory/VehicleForm";
import VehicleList from "./Components/Inventory/VehicleList";

function App() {
	const domain = /https:\/\/[^/]+/;
	const basename = process.env.PUBLIC_URL.replace(domain, "");
	return (
		<BrowserRouter basename={basename}>
			<Nav />
			<div className='container'>
				<Routes>
					<Route path='/'>
						<Route index element={<MainPage />} />
						<Route path='service/appointments/'>
							<Route index element={<AppointmentList />} />
							<Route path='history/' element={<AppointmentHistory />} />
						</Route>
						<Route path='customers/new/' element={<CustomerForm />} />
						<Route path='sales/new/' element={<SaleForm />} />
						<Route path='sales/' element={<SaleList />} />
						<Route path='salesperson/new/' element={<SalesPersonForm />} />
						<Route path='sales/history/' element={<SaleHistory />} />
						<Route path='inventory/manufacturers/new/' element={<ManufacturerForm />} />
						<Route path='inventory/manufacturers/' element={<ManufacturerList />} />
						<Route path='inventory/automobiles/' element={<AutomobilesList />} />
						<Route path='inventory/automobiles/new/' element={<AutomobileForm />} />
						<Route path='inventory/vehicles/new/' element={<VehicleForm />} />
						<Route path='inventory/vehicles/' element={<VehicleList />} />
					</Route>
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
