import { NavLink } from "react-router-dom";
import ServiceNav from "./Components/Services/ServiceNav";
import SaleNav from "./Components/Sale/SaleNav";
import InventoryNav from "./Components/Inventory/InventoryNav";
import LoginNav from "./LoginNav";
import dms192 from "./Assets/dms32.png";

function Nav() {
  return (
    <nav className='navbar navbar-expand-lg navbar-light bg-info justify-content'>
      <div className='container-fluid'>
        <div className='d-grid gap-2 d-md-flex justify-content-md-start'>
          <NavLink className='navbar-brand p-2 color-white' to='/'>
            <img src={dms192} alt='' />
          </NavLink>
          <InventoryNav />
          <SaleNav />
          <ServiceNav />
        </div>
        <LoginNav />
      </div>
    </nav>
  );
}

export default Nav;
