import AlertMessage from "./AlertMessage";

const SuccessMessage = ({ message, onReset, buttonText }) => (
	<AlertMessage
		message={message}
		onReset={onReset}
		buttonText={buttonText}
		alertClass='alert alert-success mb-3'
		buttonClass='btn btn-outline-warning'
	/>
);

export default SuccessMessage;
