const AlertMessage = ({ message, onReset, buttonText, alertClass, buttonClass }) => (
	<>
		<div className={alertClass}>{message}</div>
		<div className='d-flex justify-content-between'>
			<button onClick={onReset} className={buttonClass}>
				{buttonText}
			</button>
		</div>
	</>
);

export default AlertMessage;
