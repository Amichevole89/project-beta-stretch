import AlertMessage from "./AlertMessage";
const ErrorMessage = ({ message, onReset, buttonText }) => (
	<AlertMessage
		message={message}
		onReset={onReset}
		buttonText={buttonText}
		alertClass='alert alert-danger mb-3'
		buttonClass='btn btn-outline-warning'
	/>
);

export default ErrorMessage;
