import React, { useState, useEffect } from "react";

const url = `${process.env.REACT_APP_INVENTORY_HOST}/api/manufacturers/`;

const ListManufacturers = () => {
	const [manufacturers, setManufacturers] = useState([]);

	const fetchManufacturers = async () => {
		const response = await fetch(url);
		const allManufacturers = await response.json();
		setManufacturers(allManufacturers.manufacturers);
	};
	useEffect(() => {
		fetchManufacturers();
	}, []);
	return (
		<div className='container-fluid'>
			<h1 className='text-center p-2 m-2'>Manufacturers</h1>
			<table className='table table-hover'>
				<thead>
					<tr>
						<th>Name</th>
					</tr>
				</thead>
				<tbody>
					{manufacturers.map((make) => {
						return (
							<tr key={make.id}>
								<td>{make.name}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
};

export default ListManufacturers;
