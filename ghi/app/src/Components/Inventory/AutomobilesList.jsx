import React, { useEffect, useState, lazy, Suspense } from "react";

// import InventoryModal from "./InventoryModal";
const InventoryModal = lazy(() => import("./InventoryModal"));
const AutomobilesList = () => {
	const [automobiles, setAutomobiles] = useState([]);

	const getAutomobiles = async () => {
		const response = await fetch(`${process.env.REACT_APP_INVENTORY_HOST}/api/automobiles/`);
		if (response.ok) {
			const allAutomobiles = await response.json();
			setAutomobiles(allAutomobiles.autos);
		}
	};
	const updateAutomobilesHandler = (newAutomobile) => {
		setAutomobiles((prevState) => {
			return [...prevState, newAutomobile];
		});
	};

	useEffect(() => {
		getAutomobiles();
	}, []);

	return (
		<div className='container-fluid'>
			<h1 className='text-center'>Automobiles</h1>
			<div className='d-grid p-2 gap-2 d-md-flex justify-content-md-end'>
				<Suspense fallback={<div>Loading...</div>}>
					<InventoryModal updateAutomobiles={updateAutomobilesHandler} />
				</Suspense>
			</div>
			<table className='table table-hover '>
				<thead>
					<tr>
						<th>Picture</th>
						<th>Manufacturer</th>
						<th>Model</th>
						<th>Year</th>
						<th>Color</th>
						<th>Vin</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{automobiles.map((car) => (
						<tr key={car.vin}>
							<td>
								<img src={car.model.picture_url} alt='invalid' height='150' />
							</td>
							<td>{car.model.manufacturer.name}</td>
							<td>{car.model.name}</td>
							<td>{car.year}</td>
							<td>{car.color}</td>
							<td>{car.vin}</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

export default AutomobilesList;
