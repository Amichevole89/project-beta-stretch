import React, { useState, useCallback, useRef, useEffect } from "react";

const ManufacturerForm = React.memo(({ updateManufacturers }) => {
  const [manufacturer, setManufacturer] = useState({
    name: "",
  });

  const handleInputChange = useCallback((e) => {
    const { name, value } = e.target;
    setManufacturer((prevState) => ({ ...prevState, [name]: value }));
  }, []);

  const handleSubmit = useCallback(async (e) => {
    e.preventDefault();
    const response = await fetch(
      `${process.env.REACT_APP_INVENTORY_HOST}/api/manufacturers/`,
      {
        method: "POST",
        body: JSON.stringify(manufacturer),
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (response.ok) {
      const newManufacturer = await response.json();
      updateManufacturers(newManufacturer);
      setManufacturer({ name: "" });
      setSubmitSuccess(true);
    }
  }, [manufacturer, updateManufacturers]);

  const handleReset = useCallback(() => {
    setManufacturer({ name: "" });
    submitSuccessRef.current = false;
  }, []);

  const initialSubmitSuccess = false;
  const submitSuccessRef = useRef(initialSubmitSuccess);
  const [submitSuccess, setSubmitSuccess] = useState(initialSubmitSuccess);

  let formClass = "";
  let alertClass = "alert alert-success d-none mb-0";
  let alertContainerClass = "d-none";
  if (submitSuccess) {
    formClass = "d-none";
    alertClass = "alert alert-success mb-3";
    alertContainerClass = "";
  }

  useEffect(() => {
    submitSuccessRef.current = submitSuccess;
  }, [submitSuccess]);

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <form onSubmit={handleSubmit} className={formClass}>
              <h1>Create Manufacturer</h1>
              <div className="form-floating mb-3">
                <input
                  onChange={handleInputChange}
                  value={manufacturer.name}
                  placeholder="Name"
                  required
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="d-grid gap-2 d-md-flex justify-content-md-between">
                <button className="btn btn-outline-success">Create</button>
              </div>
            </form>
            <div className={alertContainerClass}>
              <div className={alertClass} id="success-message">
                Successfully created new Manufacturer!
              </div>
              <div className="d-flex justify-content-between">
                <button
                  disabled={submitSuccess}
                  onClick={handleReset}
                  className="btn btn-outline-warning"
                >
                  Create a second manufacturer?
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

export default ManufacturerForm;
