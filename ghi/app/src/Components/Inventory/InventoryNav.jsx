import React from "react";
import { NavLink } from "react-router-dom";

const InventoryNav = () => {
	const dropdownItems = [
		{ to: "/inventory/automobiles/", label: "List Automobiles" },
		{ to: "/inventory/vehicles/", label: "List Vehicle Models" },
		{ to: "/inventory/manufacturers/", label: "List Manufacturers" },
	];

	return (
		<>
			<div className='p-2 dropdown'>
				<NavLink
					className='link-info btn btn-secondary dropdown-toggle'
					to='#'
					role='button'
					id='dropdownMenu'
					data-bs-toggle='dropdown'
					aria-haspopup='true'
					aria-expanded='false'
				>
					Inventory
				</NavLink>

				<div className='dropdown-menu' aria-labelledby='dropdownMenu'>
					{dropdownItems.map((item, index) => (
						<React.Fragment key={index}>
							<NavLink className='dropdown-item' to={item.to}>
								{item.label}
							</NavLink>
							{index < dropdownItems.length - 1 && <li className='dropdown-divider' />}
						</React.Fragment>
					))}
				</div>
			</div>
		</>
	);
};

export default InventoryNav;
