import React, { useState } from "react";
import SuccessMessage from "../utilities/SuccessMessage";
import ErrorMessage from "../utilities/ErrorMessage";
import { FaDice } from "react-icons/fa";
const initialAutomobileState = {
	year: "",
	color: "",
	vin: "",
	model_id: "",
};
const AutomobileForm = ({ vehicleModels, updateAutomobiles }) => {
	const [automobiles, setAutomobiles] = useState(initialAutomobileState);
	const [status, setStatus] = useState({ success: false, error: false });

	const handleInputChange = (e) => {
		setAutomobiles({ ...automobiles, [e.target.name]: e.target.value });
	};

	const handleReset = () => {
		setAutomobiles(initialAutomobileState);
		setStatus({ success: false, error: false });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		const content = { ...automobiles };
		const response = await fetch(`${process.env.REACT_APP_INVENTORY_HOST}/api/automobiles/`, {
			method: "POST",
			body: JSON.stringify(content),
			headers: {
				"Content-Type": "application/json",
			},
		});

		if (response.ok) {
			const newAutomobile = await response.json();
			updateAutomobiles(newAutomobile);
			setStatus({ success: true, error: false });
			handleReset();
		} else {
			setStatus({ success: false, error: true });
		}
	};

	const generateRandomVIN = () => {
		const randomVIN = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
		setAutomobiles({ ...automobiles, vin: randomVIN.toUpperCase() });
	};

	return (
		<div className='container-fluid'>
			<div className='row'>
				<div className='offset-3 col-6'>
					<div className='shadow p-4 mt-4'>
						{!status.success && !status.error && (
							<Form
								onSubmit={handleSubmit}
								automobiles={automobiles}
								handleInputChange={handleInputChange}
								vehicleModels={vehicleModels}
								generateRandomVIN={generateRandomVIN}
							/>
						)}
						{status.success && (
							<SuccessMessage
								message='Successfully created new Automobile!'
								onReset={() => setStatus({ success: false })}
								buttonText='Create a second automobile?'
							/>
						)}
						{status.error && (
							<ErrorMessage
								message='Error, VIN already in system! Please Verify VIN #'
								onReset={() => setStatus({ error: false })}
								buttonText='Would you like to try again?'
							/>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

const Form = ({ onSubmit, automobiles, handleInputChange, vehicleModels, generateRandomVIN }) => {
	return (
		<form onSubmit={onSubmit}>
			<h1>Create Automobile</h1>
			<div className='form-floating mb-3'>
				<input
					onChange={handleInputChange}
					value={automobiles.color}
					placeholder='Color'
					required
					type='text'
					name='color'
					id='color'
					className='form-control'
				/>
				<label htmlFor='color'>Color</label>
			</div>
			<div className='form-floating mb-3'>
				<input
					onChange={handleInputChange}
					value={automobiles.vin}
					placeholder='VIN'
					required
					type='text'
					name='vin'
					id='vin'
					className='form-control'
				/>
				<label htmlFor='vin'>Vin</label>
				<button type='button' className='btn btn-outline-secondary ms-2' onClick={generateRandomVIN}>
					<FaDice />
				</button>
			</div>
			<div className='form-floating mb-3'>
				<input
					onChange={handleInputChange}
					value={automobiles.year}
					placeholder='Year'
					required
					type='number'
					name='year'
					id='year'
					className='form-control'
				/>
				<label htmlFor='year'>Year</label>
			</div>
			<div className='mb-3'>
				<select
					onChange={handleInputChange}
					value={automobiles.model_id}
					placeholder='Model ID'
					required
					name='model_id'
					id='model_id'
					className='form-control'
				>
					<option value=''>Choose a Vehicle Model</option>
					{vehicleModels.map((car) => {
						return (
							<option key={car.id} value={car.id}>
								{car.name}
							</option>
						);
					})}
				</select>
			</div>
			<div className='d-grid gap-2 d-md-flex justify-content-md-between'>
				<button className='btn btn-outline-success'>Create</button>
			</div>
		</form>
	);
};

export default AutomobileForm;
