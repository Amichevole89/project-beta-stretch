import React, { useState, useEffect } from "react";
const url = `${process.env.REACT_APP_INVENTORY_HOST}/api/models`;

const ListVehicleModels = () => {
  const [vehicleModels, setVehicleModels] = useState([]);

  const fetchVehicleModels = async () => {
    const response = await fetch(url);
    const allVehicleModels = await response.json();
    setVehicleModels(allVehicleModels.models);
  };

  useEffect(() => {
    fetchVehicleModels();
  },[]);

  return (
    <div className='container-fluid'>
      <h1 className='text-center p-2 m-2'>Vehicle Models</h1>

      <table className='table table-hover'>
        <thead>
          <tr>
            <th>Picture</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {vehicleModels.map((vehicle) => {
            return (
              <tr key={vehicle.id}>
                <td>
                  <img src={vehicle.picture_url} alt='invalid' height='150' />
                </td>
                <td>{vehicle.name}</td>
                <td>{vehicle.manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default ListVehicleModels;
