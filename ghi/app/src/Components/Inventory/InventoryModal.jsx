import React, { useState, useCallback } from "react";
import VehicleForm from "./VehicleForm";
import ManufacturerForm from "./ManufacturerForm";
import AutomobileForm from "./AutomobileForm";

const InventoryModal = ({ updateAutomobiles }) => {
	const vehicleURL = `${process.env.REACT_APP_INVENTORY_HOST}/api/models/`;
	const [vehicleModels, setVehicleModels] = useState([]);
	const updateVehicleModelsHandler = useCallback((newVehicleModel) => {
		setVehicleModels((prevState) => {
			return [...prevState, newVehicleModel];
		});
	}, []);

	const fetchVehicleModels = useCallback(async () => {
		const fetchVehiclesResponse = await fetch(vehicleURL);
		if (fetchVehiclesResponse.ok) {
			const allVehicleModels = await fetchVehiclesResponse.json();
			setVehicleModels(allVehicleModels.models);
		}
	}, [vehicleURL]);

	const manufacturerURL = `${process.env.REACT_APP_INVENTORY_HOST}/api/manufacturers/`;
	const [manufacturers, setManufacturers] = useState([]);

	const updateManufacturersHandler = useCallback((newManufacturer) => {
		setManufacturers((prevState) => {
			return [...prevState, newManufacturer];
		});
	}, []);

	const fetchManufacturers = useCallback(async () => {
		const response = await fetch(manufacturerURL);
		if (response.ok) {
			const allVehicleModels = await response.json();
			setManufacturers(allVehicleModels.manufacturers);
		}
	}, [manufacturerURL]);

	const handleCreateAutomobile = () => {
		fetchVehicleModels();
		fetchManufacturers();
	};
	return (
		<>
			<div
				className='modal fade'
				id='automobileModalToggle'
				tabIndex='-1'
				aria-labelledby='automobileModalLabel'
				aria-hidden='true'
			>
				<div className='modal-dialog modal-lg'>
					<div className='modal-content'>
						<div className='modal-header'>
							<h5 className='modal-title' id='automobileModalLabel'>
								Please complete form to continue
							</h5>
							<button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						</div>
						<div className='modal-body'>
							<div>
								<AutomobileForm updateAutomobiles={updateAutomobiles} vehicleModels={vehicleModels} />
							</div>
						</div>
						<div className='modal-footer'>
							<button
								id='vehicleModalLabel'
								data-bs-toggle='modal'
								className='btn btn-outline-success btn-md px-4 gap-3'
								data-bs-target='#vehicleModalToggle'
							>
								Create New Vehicle Model
							</button>
						</div>
					</div>
				</div>
			</div>
			<div
				className='modal fade'
				id='vehicleModalToggle'
				tabIndex='-1'
				aria-labelledby='vehicleModalLabel'
				aria-hidden='true'
			>
				<div className='modal-dialog modal-lg'>
					<div className='modal-content'>
						<div className='modal-header'>
							<h5 className='modal-title' id='vehicleModalLabel'>
								Please complete form to continue
							</h5>
							<button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						</div>
						<div className='modal-body'>
							<div>
								<VehicleForm updateVehicleModels={updateVehicleModelsHandler} manufacturers={manufacturers} />
							</div>
						</div>
						<div className='modal-footer'>
							<button
								data-bs-dismiss='modal'
								type='button'
								data-bs-target='#automobileModalToggle'
								data-bs-toggle='modal'
								className='btn btn-outline-primary'
							>
								Back to Automobile Form
							</button>
							<button
								id='technicianModal'
								data-bs-toggle='modal'
								className='btn btn-outline-success btn-md px-4 gap-3'
								data-bs-target='#manufacturerModalToggle'
							>
								Create New Manufacturer
							</button>
						</div>
					</div>
				</div>
			</div>
			<div
				className='modal fade'
				id='manufacturerModalToggle'
				tabIndex='-1'
				aria-labelledby='manufacturerModalLabel'
				aria-hidden='true'
			>
				<div className='modal-dialog modal-lg'>
					<div className='modal-content'>
						<div className='modal-header'>
							<h5 className='modal-title' id='manufacturerModalLabel'>
								Please complete form to continue
							</h5>
							<button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						</div>
						<div className='modal-body'>
							<div>
								<ManufacturerForm updateManufacturers={updateManufacturersHandler} />
							</div>
						</div>
						<div className='modal-footer'>
							<button
								data-bs-dismiss='modal'
								type='button'
								data-bs-target='#vehicleModalToggle'
								data-bs-toggle='modal'
								className='btn btn-outline-primary'
							>
								Back to Vehicle Model Form
							</button>
							<button
								data-bs-dismiss='modal'
								type='button'
								data-bs-target='#automobileModalToggle'
								data-bs-toggle='modal'
								className='btn btn-outline-primary'
							>
								Back to Automobile Form
							</button>
						</div>
					</div>
				</div>
			</div>
			<a
				className='btn btn-primary'
				data-bs-toggle='modal'
				href='#automobileModalToggle'
				onClick={handleCreateAutomobile}
				role='button'
			>
				Create New Automobile
			</a>
		</>
	);
};

export default InventoryModal;
