import React, { useState } from "react";

const VehicleForm = (props) => {
  const [vehicleModels, setVehicleModels] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: "",
  });

  const handleInputChange = (e) => {
    setVehicleModels({ ...vehicleModels, [e.target.name]: e.target.value });
  };

  const handleReset = () => {
    setVehicleModels({
      name: "",
      picture_url: "",
      manufacturer_id: "",
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const content = { ...vehicleModels };
    const response = await fetch(`${process.env.REACT_APP_INVENTORY_HOST}/api/models/`, {
      method: "POST",
      body: JSON.stringify(content),
      headers: {
        "Content-Type": "application/json",
      },
    });
    setSubmitSuccess(true);
    if (response.ok) {
      const newManufacturer = await response.json();
      props.updateVehicleModels(newManufacturer);
      handleReset();
    }
  };

  const [submitSuccess, setSubmitSuccess] = useState(false);
  let formClass = "";
  let alertClass = "alert alert-success d-none mb-0";
  let alertContainerClass = "d-none";
  if (submitSuccess) {
    formClass = "d-none";
    alertClass = "alert alert-success mb-3";
    alertContainerClass = "";
  }

  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <form onSubmit={handleSubmit} className={formClass}>
              <h1>Create Vehicle Model</h1>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={vehicleModels.name}
                  placeholder='Name'
                  required
                  type='text'
                  name='name'
                  id='name'
                  className='form-control'
                />
                <label htmlFor='name'>Name</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={vehicleModels.picture_url}
                  placeholder='Picture URL'
                  required
                  type='text'
                  name='picture_url'
                  id='picture_url'
                  className='form-control'
                />
                <label htmlFor='pictureURL'>Picture</label>
              </div>
              <div className='mb-3'>
                <select
                  onChange={handleInputChange}
                  value={vehicleModels.manufacturer_id}
                  placeholder='Manufacturer ID #'
                  required
                  name='manufacturer_id'
                  id='manufacturer_id'
                  className='form-control'
                >
                  <option value=''>Choose a Manufacturer</option>
                  {props.manufacturers.map((make) => {
                    return (
                      <option key={[make.id]} value={[make.id]}>
                        {make.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='d-grid gap-2 d-md-flex justify-content-md-between'>
                <button className='btn btn-outline-success'>Create</button>
              </div>
            </form>
            <div className={alertContainerClass}>
              <div className={alertClass} id='success-message'>
                Successfully created Vehicle Model!
              </div>
              <div className='d-flex justify-content-between'>
                <button
                  onClick={() => setSubmitSuccess(false)}
                  className='btn btn-outline-success'
                >
                  Create a second Vehicle Model?
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VehicleForm;
