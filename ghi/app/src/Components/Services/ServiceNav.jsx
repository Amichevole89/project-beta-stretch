import React from "react";
import { NavLink } from "react-router-dom";

const ServiceNav = () => {
  return (
    <div className='p-2 dropdown'>
      <NavLink
        className='link-info btn btn-secondary dropdown-toggle '
        to='#'
        role='button'
        id='dropdownMenu'
        data-bs-toggle='dropdown'
        aria-haspopup='true'
        aria-expanded='false'
      >
        Services
      </NavLink>

      <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
        <NavLink className='dropdown-item' to='/service/appointments/'>
          List of appointments
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/service/appointments/history/'>
          Appointment History
        </NavLink>
      </div>
    </div>
  );
};

export default ServiceNav;
