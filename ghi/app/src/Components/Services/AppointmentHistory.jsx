import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const url = `${process.env.REACT_APP_SERVICE_HOST}/api/appointments/`;

const AppointmentHistory = () => {
	const [appointments, setAppointments] = useState([]);
	const [input, setInput] = useState("");

	const filterAppts = input
		? appointments.filter((appt) => appt.vin.toLowerCase().includes(input.toLocaleLowerCase()))
		: appointments;

	const fetchTrueAppts = async () => {
		const response = await fetch(url);
		if (response.ok) {
			const allAppointments = await response.json();
			setAppointments(allAppointments.appointments.filter((appt) => appt.completed === true));
		}
	};

	useEffect(() => {
		fetchTrueAppts();
	}, []);

	return (
		<div className='container-fluid'>
			<h1 className='text-center p-2 m-2'>Search Appointment History</h1>
			<div className='d-grid p-1 gap-2 d-md-flex justify-content-md-end'>
				<Link className='btn btn-outline-primary' to='/service/appointments/'>
					Appointment List
				</Link>
			</div>
			<form className='form-fluid'>
				<div className='input-group mb-3'>
					<input
						onChange={(e) => setInput(e.target.value)}
						placeholder='Enter VIN Number'
						type='search'
						name='vin'
						id='vin'
						className='form-control'
						aria-describedby='searchInput'
					/>
				</div>
			</form>
			<div className='d-grid gap-2 d-md-flex justify-content-md-between'></div>
			<table className='table table-hover table-bordered'>
				<caption>**Green rows indicate VIP </caption>
				<thead>
					<tr className='table-warning'>
						<th>Vin</th>
						<th>Name</th>
						<th>Date</th>
						<th>Time</th>
						<th>Technician</th>
						<th>Reason</th>
					</tr>
				</thead>
				<tbody>
					{filterAppts &&
						filterAppts.map((appt) => (
							<tr key={appt.id} className={appt.VIP_treatment ? "table-success" : "table-danger"}>
								<td>{appt.vin}</td>
								<td>{appt.customer_name}</td>
								<td>{appt.date}</td>
								<td>{appt.time}</td>
								<td>{appt.technician.name}</td>
								<td>{appt.purpose}</td>
							</tr>
						))}
				</tbody>
			</table>
		</div>
	);
};

export default AppointmentHistory;
