function Studios() {
	const [filterValue, setFilterValue] = useState("");
	const [studios, setStudios] = useState([]);

	const getData = async () => {
		const response = await fetch(`${process.env.REACT_APP_API_HOST}/studios`);

		if (response.ok) {
			let data = await response.json();
			setStudios(data);
		}
	};

	useEffect(() => {
		getData();
	}, []);

	const handleChange = (event) => {
		setFilterValue(event.target.value);
	};

	const filteredStudio = () => {
		return studios.filter((studio) => studio.location.toLowerCase().includes(filterValue));
	};
	console.log(filterValue);

	return (
		<>
			<hr className='my-5' />
			<div className='container'>
				<h1>Discover Studios</h1>
				<input onChange={handleChange} placeholder='Search Location' />
				<div className='btn-group dropend'>
					<select
						onChange={handleChange}
						type='button'
						className='btn btn-dark dropdown-toggle'
						data-mdb-toggle='dropdown'
						aria-expanded='false'
					>
						<option className='dropdown-item' value=''>
							LOCATIONS
						</option>
						{studios.map((studio) => (
							<option className='dropdown-item' value={studio.location} key={studio.id}>
								{" "}
								{studio.location}{" "}
							</option>
						))}
					</select>
				</div>
			</div>
		</>
	);
}
