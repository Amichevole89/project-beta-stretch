import React, { useState } from "react";
import AppointmentForm from "./AppointmentForm";
import TechnicianForm from "./TechnicianForm";

const AppointmentModal = ({ updateAppointments }) => {
	const [technicians, setTechnicians] = useState([]);
	const [loading, setLoading] = useState(false);

	const updateTechniciansHandler = (newTechnician) => {
		setTechnicians((prevState) => {
			return [...prevState, newTechnician];
		});
	};

	const handleAppointmentModalClick = () => {
		setLoading(true);
		fetch(`${process.env.REACT_APP_SERVICE_HOST}/api/technicians/`)
			.then((response) => response.json())
			.then((data) => {
				setTechnicians(data.technicians);
				setLoading(false);
			})
			.catch((error) => {
				console.error("Error fetching technicians: ", error);
				setLoading(false);
			});
	};

	return (
		<>
			<div
				className='modal fade'
				id='appointmentModalToggle'
				tabIndex='-1'
				aria-labelledby='appointmentModalLabel'
				aria-hidden='true'
			>
				<div className='modal-dialog modal-lg'>
					<div className='modal-content'>
						<div className='modal-header'>
							<h5 className='modal-title' id='appointmentModalLabel'>
								Please complete form to continue
							</h5>
							<button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						</div>
						<div className='modal-body'>
							<div>
								<AppointmentForm updateAppointments={updateAppointments} technicians={technicians} loading={loading} />
							</div>
						</div>
						<div className='modal-footer'>
							<button
								id='technicianModal'
								data-bs-toggle='modal'
								className='btn btn-outline-success btn-md px-4 gap-3'
								data-bs-target='#technicianModalToggle'
							>
								Create New Technician
							</button>
						</div>
					</div>
				</div>
			</div>
			<div
				className='modal fade'
				id='technicianModalToggle'
				tabIndex='-1'
				aria-labelledby='technicianModalLabel'
				aria-hidden='true'
			>
				<div className='modal-dialog modal-lg'>
					<div className='modal-content'>
						<div className='modal-header'>
							<h5 className='modal-title' id='technicianModalLabel'>
								Please complete form to continue
							</h5>
							<button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						</div>
						<div className='modal-body'>
							<div>
								<TechnicianForm updateTechnicians={updateTechniciansHandler} />
							</div>
						</div>
						<div className='modal-footer'>
							<button
								data-bs-dismiss='modal'
								type='button'
								data-bs-target='#appointmentModalToggle'
								data-bs-toggle='modal'
								className='btn btn-outline-primary'
							>
								Back to Appointment
							</button>
						</div>
					</div>
				</div>
			</div>
			<a
				className='btn btn-primary'
				data-bs-toggle='modal'
				href='#appointmentModalToggle'
				role='button'
				onClick={handleAppointmentModalClick}
			>
				Create New Appointment
			</a>
		</>
	);
};

export default AppointmentModal;
