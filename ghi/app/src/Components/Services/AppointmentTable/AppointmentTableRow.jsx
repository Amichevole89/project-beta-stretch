import React from "react";

const AppointmentTableRow = ({ appt, onDelete, onFinished }) => {
	return (
		<tr className={appt.VIP_treatment ? "table-success" : "table-danger"}>
			{appt.VIP_treatment ? (
				<td className='text-success fw-bold'>True</td>
			) : (
				<td className='text-danger fw-bold'>False</td>
			)}
			<td>{appt.customer_name}</td>
			<td>{appt.vin}</td>
			<td>{appt.date}</td>
			<td>{appt.time}</td>
			<td>{appt.technician.name}</td>
			<td>{appt.purpose}</td>
			<td className='d-grid gap-2 d-lg-flex justify-content-md-end'>
				<button onClick={() => onDelete(appt.id)} className='btn btn-outline-danger'>
					Cancel
				</button>
				<button onClick={() => onFinished(appt.id)} className='btn btn-outline-success'>
					Finished
				</button>
			</td>
		</tr>
	);
};

export default AppointmentTableRow;
