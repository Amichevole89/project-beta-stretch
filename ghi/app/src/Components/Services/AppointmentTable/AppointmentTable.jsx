import React from "react";
import TableHeader from "./TableHeader";
import AppointmentTableRow from "./AppointmentTableRow";

const AppointmentTable = ({ appointments, onSort, onDelete, onFinished, onUpdate }) => {
	const headers = [
		{ field: "VIP_treatment", label: "VIP" },
		{ field: "customer_name", label: "Name" },
		{ field: "vin", label: "Vin" },
		{ field: "date", label: "Date" },
		{ field: "time", label: "Time" },
		{ field: "technician.name", label: "Technician" },
		{ field: "purpose", label: "Reason" },
		{ field: "actions", label: "" },
	];

	return (
		<table className='table table-hover'>
			<caption>**Green rows indicate VIP</caption>
			<thead className='table-light'>
				<TableHeader headers={headers} onSort={onSort} />
			</thead>
			<tbody>
				{appointments &&
					appointments.map((appt) => (
						<AppointmentTableRow key={appt.id} appt={appt} onDelete={onDelete} onFinished={onFinished} />
					))}
			</tbody>
		</table>
	);
};

export default AppointmentTable;
