import React, { useState } from "react";
import { AiFillCaretDown, AiFillCaretUp } from "react-icons/ai";

const TableHeader = ({ headers, onSort }) => {
	const [sortOrder, setSortOrder] = useState({ field: null, isAscending: true });

	const handleSort = (field) => {
		let isAscending = true;

		if (sortOrder.field === field) {
			isAscending = !sortOrder.isAscending;
		} else if (field) {
			isAscending = true;
		} else {
			setSortOrder({ field: null, isAscending: true });
			onSort(null, null);
			return;
		}

		setSortOrder({ field, isAscending });
		onSort(field, isAscending);
	};

	const renderSortIcon = (field) => {
		if (sortOrder.field === field) {
			return sortOrder.isAscending ? <AiFillCaretUp /> : <AiFillCaretDown />;
		}
		return null;
	};

	return (
		<tr>
			{headers.map((header) => (
				<th key={header.field} onClick={() => handleSort(header.field)}>
					{header.label} {renderSortIcon(header.field)}
				</th>
			))}
		</tr>
	);
};

export default TableHeader;
