import React, { useState, useEffect } from "react";

const AppointmentForm = (props) => {
	const [inventoryVins, setInventoryVins] = useState([]);

	const getAutomobiles = async () => {
		const response = await fetch(`${process.env.REACT_APP_INVENTORY_HOST}/api/automobiles/`);
		if (response.ok) {
			const allAutomobiles = await response.json();
			setInventoryVins(allAutomobiles.autos.map((auto) => auto.vin));
		}
	};
	const [useInventoryVin, setUseInventoryVin] = useState(false);
	const handleCheckboxChange = (e) => {
		setUseInventoryVin(e.target.checked);
	};
	useEffect(() => {
		getAutomobiles();
	}, [useInventoryVin]);
	const [appointment, setAppointment] = useState({
		customer_name: "",
		VIP_treatment: "",
		vin: "",
		purpose: "",
		date: "",
		time: "",
		technician: "",
	});

	const handleInputChange = (e) => {
		setAppointment({ ...appointment, [e.target.name]: e.target.value });
	};

	const handleReset = () => {
		setAppointment({
			customer_name: "",
			VIP_treatment: "",
			vin: "",
			purpose: "",
			date: "",
			time: "",
			technician: "",
		});
	};
	const handleSubmit = async (e) => {
		e.preventDefault();
		const content = { ...appointment };
		const response = await fetch(`${process.env.REACT_APP_SERVICE_HOST}/api/appointments/`, {
			method: "POST",
			body: JSON.stringify(content),
			headers: { "Content-Type": "application/json" },
		});
		setSubmitSuccess(true);
		if (response.ok) {
			const newAppointment = await response.json();
			props.updateAppointments(newAppointment);
			handleReset();
		}
	};

	const [submitSuccess, setSubmitSuccess] = useState(false);
	let formClass = "";
	let alertClass = "alert alert-success d-none mb-0";
	let alertContainerClass = "d-none";
	if (submitSuccess) {
		formClass = "d-none";
		alertClass = "alert alert-success mb-3";
		alertContainerClass = "";
	}

	return (
		<div className='container-fluid'>
			{props.loading && (
				<div>
					{setTimeout(
						() => (
							<h1>Loading...</h1>
						),
						10000
					)}
				</div>
			)}
			<div className='row'>
				<div className='offset-3 col-6'>
					<div className='shadow p-4 mt-4'>
						<form onSubmit={handleSubmit} className={formClass}>
							<h1>Create Appointment</h1>
							<div className='form-floating mb-3'>
								<input
									onChange={handleInputChange}
									value={appointment.customer_name}
									placeholder='customer_name'
									required
									type='text'
									name='customer_name'
									id='customer_name'
									className='form-control shadow-lg'
								/>
								<label htmlFor='customer_name'>Name</label>
							</div>
							<div className='form-check mb-3'>
								<input
									className='form-check-input'
									type='checkbox'
									value={useInventoryVin}
									onChange={handleCheckboxChange}
									id='useInventoryVin'
								/>
								<label className='form-check-label' htmlFor='useInventoryVin'>
									Use VIN from inventory
								</label>
							</div>
							{useInventoryVin ? (
								<div className='form-floating mb-3'>
									<select
										onChange={handleInputChange}
										value={appointment.vin}
										placeholder='Model'
										required
										name='vin'
										id='vin'
										className='form-control text-success shadow-lg'
									>
										<option value=''>Choose a VIN from inventory</option>
										{inventoryVins.map((vin) => {
											return (
												<option key={vin} value={vin}>
													{vin}
												</option>
											);
										})}
									</select>
									<label htmlFor='vin'>Vin</label>
								</div>
							) : (
								<div className='form-floating mb-3'>
									<input
										onChange={handleInputChange}
										value={appointment.vin}
										placeholder='Model'
										required
										type='text'
										name='vin'
										id='vin'
										className='form-control text-success shadow-lg'
									/>
									<label htmlFor='vin'>Vin</label>
								</div>
							)}

							<div className='form-floating mb-3'>
								<input
									onChange={handleInputChange}
									value={appointment.purpose}
									placeholder='purpose'
									required
									type='text'
									name='purpose'
									id='purpose'
									className='form-control shadow-lg'
								/>
								<label htmlFor='purpose'>purpose</label>
							</div>
							<div className='form-floating mb-3'>
								<input
									onChange={handleInputChange}
									value={appointment.date}
									placeholder='purpose'
									required
									type='date'
									name='date'
									id='date'
									className='form-control shadow-lg'
								/>
								<label htmlFor='date'>date</label>
							</div>
							<div className='form-floating mb-3'>
								<input
									onChange={handleInputChange}
									value={appointment.time}
									placeholder='time'
									required
									type='time'
									name='time'
									id='time'
									className='form-control text-primary fw-bold shadow-lg'
								/>
								<label htmlFor='time'>time</label>
							</div>
							<div className='mb-3'>
								<select
									onChange={handleInputChange}
									value={appointment.technician}
									placeholder='Technician'
									required
									type='text'
									name='technician'
									id='technician'
									className='form-control fw-bold text-warning shadow-lg'
								>
									<option value=''>Choose a technician</option>
									{props.technicians.map((technician) => {
										return (
											<option key={[technician.id]} value={[technician.id]}>
												{technician.name}
											</option>
										);
									})}
								</select>
							</div>
							<div className='d-grid gap-2 d-md-flex justify-content-md-between'>
								<button className='btn btn-outline-success'>Create</button>
							</div>
						</form>
						<div className={alertContainerClass}>
							<div className={alertClass} id='success-message'>
								Successfully created Appointment!
							</div>
							<div className='d-flex justify-content-between'>
								<button onClick={() => setSubmitSuccess(false)} className='btn btn-outline-warning'>
									Create a second appointment?
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default AppointmentForm;
