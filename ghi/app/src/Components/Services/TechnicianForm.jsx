import React, { useState } from "react";

const TechnicianForm = (props) => {
	const [technician, setTechnician] = useState({
		name: "",
		employee_number: "",
	});

	const handleInputChange = (e) => {
		setTechnician({ ...technician, [e.target.name]: e.target.value });
	};

	const handleReset = () => {
		setTechnician({
			name: "",
			employee_number: "",
		});
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		const content = { ...technician };
		const response = await fetch(`${process.env.REACT_APP_SERVICE_HOST}/api/technicians/`, {
			method: "POST",
			body: JSON.stringify(content),
			headers: { "Content-Type": "application/json" },
		});
		setSubmitSuccess(true);
		if (response.ok) {
			const newTechnician = await response.json();
			props.updateTechnicians(newTechnician);
			handleReset();
		}
	};

	const [submitSuccess, setSubmitSuccess] = useState(false);
	let formClass = "";
	let alertClass = "alert alert-success d-none mb-0";
	let alertContainerClass = "d-none";
	if (submitSuccess) {
		formClass = "d-none";
		alertClass = "alert alert-success mb-3";
		alertContainerClass = "";
	}

	return (
		<div className='container-fluid'>
			<div className='row'>
				<div className='offset-3 col-6'>
					<div className='shadow p-4 mt-4'>
						<h1>Create New Technician</h1>
						<form onSubmit={handleSubmit} className={formClass}>
							<div className='form-floating mb-3'>
								<input
									onChange={handleInputChange}
									value={technician.name}
									placeholder='Name'
									required
									type='text'
									name='name'
									id='name'
									className='form-control'
								/>
								<label htmlFor='name'>Name</label>
							</div>
							<div className='form-floating mb-3'>
								<input
									onChange={handleInputChange}
									value={technician.employee_number}
									placeholder='Employee Number'
									required
									type='number'
									name='employee_number'
									id='employee_number'
									className='form-control'
								/>
								<label htmlFor='name'>Employee Number</label>
							</div>

							<div className='d-grid gap-2 d-md-flex justify-content-md-end'>
								<button className='btn btn-outline-success'>Create</button>
							</div>
						</form>
						<div className={alertContainerClass}>
							<div className={alertClass} id='success-message'>
								Successfully created new technician!
							</div>
							<div className='d-flex justify-content-between'>
								<button onClick={() => setSubmitSuccess(false)} className='btn btn-outline-warning  '>
									Create another technician?
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default TechnicianForm;

// nameTextListener(e){
//   let field = e.target.name;
//   console.log(field, ':', e.target.value)
//   this.setState({[field]: e.target.value})
// }

// const [technician, setTechnician] = useState({});
// const [name, setName] = useState('')
// const [employee_number, setEmployee_number] = useState('')
// const [technicianList, setTechnicianList] = useState([])

// const techniciansUrl = "http://localhost:8080/api/technicians/";
// const fetchData = async () => {
//   try {
//     const response = await fetch(techniciansUrl);
//     if (response.ok) {
//       const data = await response.json();
//       setTechniciansList(data.technicians)

//     }
//   } catch (e) {
//     console.log("error", e);
//   }
// };

// useEffect(() => {
//   fetchData();
// }, []);
