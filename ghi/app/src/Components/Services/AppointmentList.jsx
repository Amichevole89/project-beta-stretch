import React, { useEffect, useState } from "react";
import AppointmentModal from "./AppointmentModal";
import { Link } from "react-router-dom";
import AppointmentTable from "./AppointmentTable/AppointmentTable";
import ErrorMessage from "../utilities/ErrorMessage";
const url = `${process.env.REACT_APP_SERVICE_HOST}/api/appointments/`;

const AppointmentList = () => {
	const [appointments, setAppointments] = useState([]);
	const [errorMessage, setErrorMessage] = useState("");

	const fetchFalseAppts = async () => {
		try {
			const response = await fetch(url);
			if (response.ok) {
				const allAppointments = await response.json();
				setAppointments(allAppointments.appointments.filter((appt) => appt.completed === false));
			} else {
				setErrorMessage("Unable to fetch appointments");
			}
		} catch (error) {
			setErrorMessage("An error occurred while fetching appointments");
		}
	};

	const updateAppointmentsHandler = (newAppointment) => {
		setAppointments((prevAppointments) => {
			return [...prevAppointments, newAppointment];
		});
	};

	useEffect(() => {
		fetchFalseAppts();
	}, []);

	const deleteAppointment = async (id) => {
		try {
			await fetch(`${url}${id}/`, {
				method: "DELETE",
				headers: { "Content-Type": "application/json" },
			});
			fetchFalseAppts(setAppointments);
		} catch (error) {
			setErrorMessage("Error deleting appointment.");
		}
	};

	const finishAppointment = async (id) => {
		try {
			const response = await fetch(`${url}${id}/`, {
				method: "PUT",
				body: JSON.stringify({ completed: true }),
				headers: {
					"Content-Type": "application/json",
				},
			});
			if (response.ok) {
				fetchFalseAppts(setAppointments);
			} else {
				setErrorMessage("Error finishing appointment.");
			}
		} catch (error) {
			setErrorMessage("Error connecting to the server.");
		}
	};

	const handleSort = (field, isAscending) => {
		const sortedAppointments = [...appointments].sort((a, b) => {
			const valueA = a[field];
			const valueB = b[field];
			let comparison = 0;
			if (valueA > valueB) {
				comparison = 1;
			} else if (valueA < valueB) {
				comparison = -1;
			}
			return isAscending ? comparison : comparison * -1;
		});
		setAppointments(sortedAppointments);
	};

	return (
		<div className='container-fluid'>
			<h1 className='text-center p-2 m-2'>Appointments</h1>
			<div className='d-grid gap-2 d-md-flex justify-content-md-between'></div>
			<div className='d-grid p-2 gap-2 d-md-flex justify-content-md-between'>
				<Link className='btn btn-outline-primary' to='/service/appointments/history/'>
					Appointment History
				</Link>
				<AppointmentModal updateAppointments={updateAppointmentsHandler} />
			</div>
			{errorMessage && <ErrorMessage message={errorMessage} />}
			<AppointmentTable
				appointments={appointments}
				onSort={handleSort}
				onDelete={deleteAppointment}
				onFinished={finishAppointment}
			/>
		</div>
	);
};

export default AppointmentList;
