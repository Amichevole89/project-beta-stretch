import React from "react";
import { NavLink } from "react-router-dom";

const menuItems = [
	{ label: "Sales search", path: "/sales/history/" },
	{ label: "List of sales", path: "/sales/" },
	{ label: "Create sales record", path: "/sales/new/" },
	{ label: "Add salesperson", path: "/salesperson/new/" },
	{ label: "Add customer", path: "/customers/new/" },
];

const SaleNav = () => {
	return (
		<div className='p-2 dropdown'>
			<NavLink
				className='link-info btn btn-secondary dropdown-toggle'
				to='#'
				role='button'
				id='dropdownMenu'
				data-bs-toggle='dropdown'
				aria-haspopup='true'
				aria-expanded='false'
			>
				Sales
			</NavLink>
			<div className='dropdown-menu' aria-labelledby='dropdownMenu'>
				{menuItems.map((item) => (
					<React.Fragment key={item.path}>
						<li className='dropdown-divider' />
						<NavLink className='dropdown-item' to={item.path}>
							{item.label}
						</NavLink>
					</React.Fragment>
				))}
			</div>
		</div>
	);
};

export default SaleNav;
