import { useState, useEffect } from "react";

function SalesHistory() {
	const [salesperson, setSalesPerson] = useState([]);
	const [salerecords, setSaleRecData] = useState([]);
	const [selectedSalesPerson, setSelectedSalesPerson] = useState("");
	const [isLoading, setIsLoading] = useState(true);

	async function handleSalesPersonChange(e) {
		const value = e.target.value;
		setSelectedSalesPerson(value);
		const getsalesURL = `${process.env.REACT_APP_SALES_HOST}/api/salesperson/${value}/salesrecord/`;
		const getsaleresponse = await fetch(getsalesURL);
		const data = await getsaleresponse.json();
		setSaleRecData(data.salerecords);
	}

	async function getSalesPerson() {
		try {
			const salespersonURL = `${process.env.REACT_APP_SALES_HOST}/api/salesperson/`;
			const salespersonresponse = await fetch(salespersonURL);
			if (salespersonresponse.ok) {
				const salesData = await salespersonresponse.json();
				setSalesPerson(salesData.salesperson);
			} else {
				throw new Error("Failed to fetch salespersons");
			}
		} catch (error) {
			console.error(error);
		} finally {
			setIsLoading(false);
		}
	}

	async function getSaleRecData() {
		try {
			const url = `${process.env.REACT_APP_SALES_HOST}/api/salesrecord/`;
			const response = await fetch(url);
			if (response.ok) {
				const data = await response.json();
				setSaleRecData(data.salerecords);
			} else {
				throw new Error("Failed to fetch sale records");
			}
		} catch (error) {
			console.error(error);
		} finally {
			setIsLoading(false);
		}
	}

	useEffect(() => {
		getSaleRecData();
		getSalesPerson();
	}, []);

	if (isLoading) {
		return <div>Loading...</div>;
	}

	return (
		<>
			<div className='form-floating mb-3'>
				<select
					onChange={handleSalesPersonChange}
					value={selectedSalesPerson}
					name='salesperson'
					id='salesperson'
					className='form-select'
				>
					<option value=''>Select salesperson</option>
					{salesperson.map((saleperson) => {
						return (
							<option key={saleperson.id} value={saleperson.id}>
								{saleperson.name}
							</option>
						);
					})}
				</select>
			</div>
			<table className='table table-striped'>
				<thead>
					<tr>
						<th>Salesperson</th>
						<th>Employee Number</th>
						<th>Customer</th>
						<th>VIN</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					{salerecords &&
						salerecords.map((salerecord) => {
							return (
								<tr key={salerecord.id}>
									<td>{salerecord.salesperson.name}</td>
									<td>{salerecord.salesperson.employee_number}</td>
									<td>{salerecord.customer.name}</td>
									<td>{salerecord.automobile.vin}</td>
									<td>{salerecord.price}</td>
								</tr>
							);
						})}
				</tbody>
			</table>
		</>
	);
}

export default SalesHistory;
