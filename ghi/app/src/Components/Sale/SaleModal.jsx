import React, { useCallback, useState } from "react";
import SaleForm from "./SaleForm";
import CustomerForm from "./CustomerForm";
import SalesPersonForm from "./SalesPersonForm";

const customerURL = `${process.env.REACT_APP_SALES_HOST}/api/customers/`;
const automobileURL = `${process.env.REACT_APP_INVENTORY_HOST}/api/automobiles/`;
const salespersonURL = `${process.env.REACT_APP_SALES_HOST}/api/salesperson/`;

const SaleModal = ({ updateSales }) => {
	const [dataLoaded, setDataLoaded] = useState(false);
	const [customers, setCustomers] = useState(null);
	const [automobiles, setAutomobiles] = useState([]);
	const [salespersons, setSalesPersons] = useState(null);

	const handleLoadData = () => {
		Promise.all([fetch(customerURL), fetch(automobileURL), fetch(salespersonURL)])
			.then(([customerResponse, automobileResponse, salespersonResponse]) =>
				Promise.all([customerResponse.json(), automobileResponse.json(), salespersonResponse.json()])
			)
			.then(([customerData, automobileData, salespersonData]) => {
				setCustomers(customerData.customers);
				setAutomobiles(automobileData.autos);
				setSalesPersons(salespersonData.salesperson);
				setDataLoaded(true);
			})
			.catch((error) => console.error(error));
		setDataLoaded(true);
	};

	const updateCustomerHandler = useCallback((newCustomer) => {
		setCustomers((prevState) => {
			return [...prevState, newCustomer];
		});
	}, []);

	const updateSalesPersonHandler = useCallback((newSalesperson) => {
		setSalesPersons((prevState) => {
			return [...prevState, newSalesperson];
		});
	}, []);

	return (
		<>
			<div
				className='modal fade'
				id='saleModalToggle'
				tabIndex='-1'
				aria-labelledby='saleModalLabel'
				aria-hidden='true'
			>
				<div className='modal-dialog modal-lg'>
					<div className='modal-content'>
						<div className='modal-header'>
							<h5 className='modal-title' id='saleModalLabel'>
								Please complete form to continue
							</h5>
							<button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						</div>
						<div className='modal-body'>
							{dataLoaded ? (
								<div>
									<SaleForm
										updateSales={updateSales}
										automobiles={automobiles}
										salespersons={salespersons}
										customers={customers}
									/>
								</div>
							) : (
								<div>Loading...</div>
							)}
						</div>
						<div className='modal-footer'>
							<button
								id='customerModalLabel'
								data-bs-toggle='modal'
								className='btn btn-outline-success btn-md px-4 gap-3'
								data-bs-target='#customerModalToggle'
							>
								Create Customer
							</button>
						</div>
					</div>
				</div>
			</div>
			<div
				className='modal fade'
				id='customerModalToggle'
				tabIndex='-1'
				aria-labelledby='customerModalLabel'
				aria-hidden='true'
			>
				<div className='modal-dialog modal-lg'>
					<div className='modal-content'>
						<div className='modal-header'>
							<h5 className='modal-title' id='customerModalLabel'>
								Please complete form to continue
							</h5>
							<button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						</div>
						<div className='modal-body'>
							{dataLoaded ? (
								<div>
									<CustomerForm updateCustomers={updateCustomerHandler} />
								</div>
							) : (
								<div>Loading...</div>
							)}
						</div>
						<div className='modal-footer'>
							<button
								data-bs-dismiss='modal'
								type='button'
								data-bs-target='#saleModalToggle'
								data-bs-toggle='modal'
								className='btn btn-outline-primary'
							>
								Back to Sale Form
							</button>
							<button
								id='technicianModal'
								data-bs-toggle='modal'
								className='btn btn-outline-success btn-md px-4 gap-3'
								data-bs-target='#salesPersonModalToggle'
							>
								Create New SalesPerson
							</button>
						</div>
					</div>
				</div>
			</div>
			<div
				className='modal fade'
				id='salesPersonModalToggle'
				tabIndex='-1'
				aria-labelledby='salesPersonModalLabel'
				aria-hidden='true'
			>
				<div className='modal-dialog modal-lg'>
					<div className='modal-content'>
						<div className='modal-header'>
							<h5 className='modal-title' id='salesPersonModalLabel'>
								Please complete form to continue
							</h5>
							<button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
						</div>
						<div className='modal-body'>
							{dataLoaded ? (
								<div>
									<SalesPersonForm updateSalespersons={updateSalesPersonHandler} />
								</div>
							) : (
								<div>Loading...</div>
							)}
						</div>
						<div className='modal-footer'>
							<button
								data-bs-dismiss='modal'
								type='button'
								data-bs-target='#customerModalToggle'
								data-bs-toggle='modal'
								className='btn btn-outline-primary'
							>
								Back to Customer Form
							</button>
							<button
								data-bs-dismiss='modal'
								type='button'
								data-bs-target='#saleModalToggle'
								data-bs-toggle='modal'
								className='btn btn-outline-primary'
							>
								Back to Sale Form
							</button>
						</div>
					</div>
				</div>
			</div>
			<a
				className='btn btn-primary'
				onClick={handleLoadData}
				data-bs-toggle='modal'
				href='#saleModalToggle'
				role='button'
			>
				Create Sale
			</a>
		</>
	);
};

export default SaleModal;
