import React, { useState } from "react";

const CustomerForm = (props) => {
  const [customer, setCustomer] = useState({
    name: "",
    address: "",
    phone: "",
  });
  const handleInputChange = (e) => {
    setCustomer({ ...customer, [e.target.name]: e.target.value });
  };

  const handleReset = () => {
    setCustomer({
      name: "",
      address: "",
      phone: "",
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const content = { ...customer };
    const response = await fetch(`${process.env.REACT_APP_SALES_HOST}/api/customers/`, {
      method: "POST",
      body: JSON.stringify(content),
      headers: { "Content-Type": "application/json" },
    });
    setSubmitSuccess(true);
    if (response.ok) {
      const newCustomer = await response.json();
      props.updateCustomers(newCustomer);
      handleReset();
    }
  };

  const [submitSuccess, setSubmitSuccess] = useState(false);
  let formClass = "";
  let alertClass = "alert alert-success d-none mb-0";
  let alertContainerClass = "d-none";
  if (submitSuccess) {
    formClass = "d-none";
    alertClass = "alert alert-success mb-3";
    alertContainerClass = "";
  }

  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Create New Customer</h1>
            <form className={formClass} onSubmit={handleSubmit}>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={customer.name}
                  placeholder='Name'
                  required
                  name='name'
                  id='name'
                  className='form-control'
                  type='text'
                />
                <label htmlFor='name'>Name</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={customer.address}
                  placeholder='Address'
                  required
                  name='address'
                  id='address'
                  className='form-control'
                  type='text'
                />
                <label htmlFor='address'>Address</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={customer.phone}
                  placeholder='Phone Number'
                  required
                  name='phone'
                  id='phone'
                  className='form-control'
                  type='text'
                />
                <label htmlFor='phone'>Phone Number</label>
              </div>
              <div className='d-grid gap-2 d-md-flex justify-content-md-end'>
                <button className='btn btn-outline-success'>Create</button>
              </div>
            </form>
            <div className={alertContainerClass}>
              <div className={alertClass} id='success-message'>
                Successfully created new customer!
              </div>
              <div className='d-flex justify-content-between'>
                <button
                  onClick={() => setSubmitSuccess(false)}
                  className='btn btn-outline-warning'
                >
                  Create another customer?
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CustomerForm;
