import React, { useEffect, useState, useCallback } from "react";
import { Link } from "react-router-dom";
import SaleModal from "./SaleModal";

const SaleList = () => {
	const [sales, setSales] = useState([]);
	const getSales = async () => {
		const response = await fetch(`${process.env.REACT_APP_SALES_HOST}/api/sales/`);
		if (response.ok) {
			const data = await response.json();
			setSales(data.sales);
		}
	};

	const updateSalesHandler = useCallback((newSale) => {
		setSales((prevState) => {
			return [...prevState, newSale];
		});
	}, []);

	useEffect(() => {
		getSales();
	}, []);

	return (
		<div className='container-fluid'>
			<h1 className='text-center p-2 m-2'>Sales</h1>
			<div className='d-grid gap-2 d-md-flex justify-content-md-between'>
				<Link className='btn btn-outline-primary' to='/sales/'>
					Sale Records
				</Link>
				<SaleModal updateSales={updateSalesHandler} />
			</div>
			<table className='table table-hover'>
				<caption>list of all sales</caption>
				<thead className='table-light'>
					<tr>
						<th>Salesperson</th>
						<th>Employee Number</th>
						<th>Customer</th>
						<th>VIN</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					{sales.length > 0 ? (
						sales.map((sale) => (
							<tr key={sale.id}>
								<td>{sale.salesperson.name}</td>
								<td>{sale.salesperson.employee_number}</td>
								<td>{sale.customer.name}</td>
								<td>{sale.automobile.vin}</td>
								<td>{sale.price}</td>
							</tr>
						))
					) : (
						<tr>
							<td colSpan='5' className='text-center'>
								No sales found.
							</td>
						</tr>
					)}
				</tbody>
			</table>
		</div>
	);
};

export default SaleList;
