import React, { useState, useRef } from "react";

const SaleForm = ({ updateSales, automobiles, salespersons, customers }) => {
	const [sale, setSale] = useState({
		salesperson: "",
		customer: "",
		automobile: "",
		price: "",
	});
	const handleInputChange = (e) => {
		setSale({ ...sale, [e.target.name]: e.target.value });
	};
	const formRef = useRef(null);
	const handleSubmit = async (e) => {
		e.preventDefault();
		const content = { ...sale };
		const response = await fetch(`${process.env.REACT_APP_SALES_HOST}/api/sales/`, {
			method: "POST",
			body: JSON.stringify(content),
			headers: { "Content-Type": "application/json" },
		});
		if (response.ok) {
			const newSale = await response.json();
			updateSales(newSale);
			setSale({ salesperson: "", customer: "", automobile: "", price: "" });
			formRef.current.reset();
		}
	};

	const [submitSuccess, setSubmitSuccess] = useState(false);
	let formClass = "";
	let alertClass = "alert alert-success d-none mb-0";
	let alertContainerClass = "d-none";
	if (submitSuccess) {
		formClass = "d-none";
		alertClass = "alert alert-success mb-3";
		alertContainerClass = "";
	}

	return (
		<div className='container-fluid'>
			<div className='row'>
				<div className='offset-3 col-6'>
					<div className='shadow p-4 mt-4'>
						<h1>Create New Sale</h1>
						<form onSubmit={handleSubmit} ref={formRef} className={formClass}>
							<div className='form-floating mb-3'>
								<select
									onChange={handleInputChange}
									required
									name='salesperson'
									id='salesperson'
									value={sale.salesperson}
									className='form-select'
								>
									<option value=''>Choose a sales person</option>
									{salespersons?.map((person) => {
										return (
											<option key={person.id} value={person.id}>
												{person.name}
											</option>
										);
									})}
								</select>
							</div>
							<div className='form-floating mb-3'>
								<select
									onChange={handleInputChange}
									required
									name='customer'
									id='customer'
									value={sale.customer}
									className='form-select'
								>
									<option value=''>Choose a customer</option>
									{customers?.map((customer) => {
										return (
											<option key={customer.id} value={customer.id}>
												{customer.name}
											</option>
										);
									})}
								</select>
							</div>
							<div className='form-floating mb-3'>
								<select
									onChange={handleInputChange}
									required
									name='automobile'
									id='automobile'
									value={sale.automobile}
									className='form-select'
								>
									<option value=''>Choose an automobile</option>
									{automobiles?.map((automobile) => {
										return (
											<option key={automobile.id} value={automobile.id}>
												{automobile.model.name}
											</option>
										);
									})}
								</select>
							</div>
							<div className='form-floating mb-3'>
								<input
									onChange={handleInputChange}
									placeholder='enter sale price'
									required
									type='text'
									name='price'
									id='price'
									value={sale.price}
									className='form-control'
								/>
							</div>
							<div className='d-grid gap-2 d-md-flex justify-content-md-end'>
								<button className='btn btn-outline-success'>Create</button>
							</div>
						</form>
						<div className={alertContainerClass}>
							<div className={alertClass} id='success-message'>
								Successfully created new Sale!
							</div>
							<div className='d-flex justify-content-between'>
								<button onClick={() => setSubmitSuccess(false)} className='btn btn-outline-warning'>
									Create another sale?
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default SaleForm;
