import React, { useState } from "react";

const SalesPersonForm = ({ updateSalespersons }) => {
	const [{ name, employee_number }, setSalesperson] = useState({
		name: "",
		employee_number: "",
	});

	const handleInputChange = (e) => {
		setSalesperson((prevState) => ({
			...prevState,
			[e.target.name]: e.target.value,
		}));
	};

	const handleReset = () => {
		setSalesperson({
			name: "",
			employee_number: "",
		});
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		const content = { ...{ name, employee_number } };
		const response = await fetch(`${process.env.REACT_APP_SALES_HOST}/api/salesperson/`, {
			method: "POST",
			body: JSON.stringify(content),
			headers: { "Content-Type": "application/json" },
		});

		if (response.ok) {
			const newSalesperson = await response.json();
			updateSalespersons(newSalesperson);
			handleReset();
			setSubmitSuccess(true);
		}
	};

	const [submitSuccess, setSubmitSuccess] = useState(false);

	return (
		<div className='container-fluid'>
			<div className='row'>
				<div className='offset-3 col-6'>
					<div className='shadow p-4 mt-4'>
						<h1>Create New Salesperson</h1>
						<form onSubmit={handleSubmit}>
							<div className='form-floating mb-3'>
								<input
									onChange={handleInputChange}
									value={name}
									placeholder='Name'
									required
									name='name'
									id='name'
									className='form-control'
									type='text'
								/>
								<label htmlFor='name'>Name</label>
							</div>
							<div className='form-floating mb-3'>
								<input
									onChange={handleInputChange}
									value={employee_number}
									placeholder='Employee Number'
									required
									name='employee_number'
									id='employee_number'
									className='form-control'
									type='text'
								/>
								<label htmlFor='employee_number'>Employee Number</label>
							</div>
							<div className='d-grid gap-2 d-md-flex justify-content-md-end'>
								<button className='btn btn-outline-success'>Create</button>
							</div>
						</form>
						{submitSuccess && (
							<div className='alert alert-success mb-3' id='success-message'>
								Successfully created a new Salesperson!
							</div>
						)}
						<div className={`d-flex justify-content-between ${submitSuccess ? "" : "d-none"}`}>
							<button onClick={() => setSubmitSuccess(false)} className='btn btn-outline-warning'>
								Create another Salesperson?
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default SalesPersonForm;
