import React from "react";
import { NavLink } from "react-router-dom";

const SalesNav = () => {
  return (
    <div className='p-2 dropdown'>
      <NavLink
        className='link-info btn btn-secondary dropdown-toggle'
        to='#'
        role='button'
        id='dropdownMenu'
        data-bs-toggle='dropdown'
        aria-haspopup='true'
        aria-expanded='false'
      >
        Sales
      </NavLink>
      <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/sales/saleshistory/'>
          Sales search
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/sales/salesrecords/'>
          List of sales
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/sales/salesrecords/new'>
          Create sales record
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/sales/salesperson/new/'>
          Add salesperson
        </NavLink>
        <li className='dropdown-divider' />
        <NavLink className='dropdown-item' to='/sales/customers/new/'>
          Add customer
        </NavLink>
      </div>
    </div>
  );
};

export default SalesNav;

// import React from "react";

// function SalesNav() {
//   return (
//     <div className='p-2 dropdown'>
//       <NavLink
//         className='link-info btn btn-secondary dropdown-toggle'
//         to='#'
//         role='button'
//         id='dropdownMenu'
//         data-bs-toggle='dropdown'
//         aria-haspopup='true'
//         aria-expanded='false'
//       >
//         Sales
//       </NavLink>
//       <div className='dropdown-menu' aria-labelledby='dropdownMenu'>
//         <li className='dropdown-divider' />
//         <NavLink className='dropdown-item' to='/sales/saleshistory/'>
//           Sales search
//         </NavLink>
//         <li className='dropdown-divider' />
//         <NavLink className='dropdown-item' to='/sales/salesrecords/'>
//           List of sales
//         </NavLink>
//         <li className='dropdown-divider' />
//         <NavLink className='dropdown-item' to='/sales/salesrecords/new'>
//           Create sales record
//         </NavLink>
//         <li className='dropdown-divider' />
//         <NavLink className='dropdown-item' to='/sales/salesperson/new/'>
//           Add salesperson
//         </NavLink>
//         <li className='dropdown-divider' />
//         <NavLink className='dropdown-item' to='/sales/customers/new/'>
//           Add customer
//         </NavLink>
//       </div>
//     </div>
//   );
// }
// export default SalesNav
