function MainPage() {
  return (
    <div className='d-grid gap-2 d-md-flex justify-content-md-center'>
      <div className='py-5 my-5 text-center'>
        <h1 className='display-4 fw-bold'>Dealership Solutions</h1>
        <img
          src='https://genhq.com/wp-content/uploads/2018/09/CarDealers.gif'
          alt=''
        />
        <div className='col-lg-6 mx-auto'>
          <p className='lead mb-4'>
            The premiere solution for automobile dealership management!
          </p>
        </div>
        <div className='fixed-bottom mb-2 z-index-0'>
          copyright this is now my footer
        </div>
      </div>
    </div>
  );
}

export default MainPage;
